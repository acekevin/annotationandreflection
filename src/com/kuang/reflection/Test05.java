package com.kuang.reflection;
/*
* 验证JAVA类加载过程
* */
public class Test05 {
    public static void main(String[] args) {
        A a = new A();
        System.out.println(A.m);
    }
}

class A{
    static {
        System.out.println("A类静态代码初始快");
        m = 300;
    }
    static int m = 100;

    public A() {
        System.out.println("A类的无参构造初始化 ");
    }
}