package com.kuang.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test10 {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        test01();
        test02();
        test03();

    }
    public static void test01() {
        // 普通方式调用
        User user = new User();
        long startTime = System.currentTimeMillis();

        for (int i = 0; i < 100000000; i++) {
            user.getName();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("普通方式调用1亿次耗时："  + (endTime-startTime) + "ms");
        // 反射方式调用
        
        // 反射方式调用，开启可见性
    }

    // 反射方式调用
    public static void test02() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // 普通方式调用
        User user = new User();
        Class c1 = user.getClass();
        Method getName = c1.getDeclaredMethod("getName", null);
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 100000000; i++) {
            getName.invoke(user, null);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("反射方式调用1亿次耗时："  + (endTime-startTime) + "ms");
    }

    /*
    * 反射方式调用，开启可见性
    */
    public static void test03() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // 普通方式调用
        User user = new User();
        Class c1 = user.getClass();
        Method getName = c1.getDeclaredMethod("getName", null);

        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 100000000; i++) {
            getName.setAccessible(true);
            getName.invoke(user, null);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("反射方式(开启可见性)调用1亿次耗时："  + (endTime-startTime) + "ms");
    }
}
