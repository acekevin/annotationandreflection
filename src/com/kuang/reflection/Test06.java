package com.kuang.reflection;

public class Test06 {
    static {
        System.out.println("Main类被加载");
    }

    public static void main(String[] args) throws ClassNotFoundException {
        //Son son = new Son();

        // 通过反射也会产生主动引用
        //Class.forName("com.kuang.reflection.Son");
        // 不会产生主动类的引用
        //
        //System.out.println(Son.b);
        //Son[] array = new Son[10];
        System.out.println(Son.M);
    }
}

class Father {
    static int b = 2;
    static {
        System.out.println("父类被加载");
    }
}

class Son extends Father {
    static {
        System.out.println("子类被加载");
    }

    static int m = 100;
    static final int M = 4000;
}
