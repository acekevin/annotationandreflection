package com.kuang.annotation;

import java.lang.annotation.*;

@MyAnnotation
public class Test02 {
    @MyAnnotation
    public void test() {

    }
}

//定义一个注解
@Target({ElementType.METHOD, ElementType.TYPE})
//Retention 表示注解在什么地方有效
//Runtime > class > sources
@Retention(RetentionPolicy.RUNTIME)
// 表示该注解
@Documented
@interface MyAnnotation {

}
